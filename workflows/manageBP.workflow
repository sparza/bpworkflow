{
	"contents": {
		"5b8b9efe-b61e-4716-91c0-59cc8a252770": {
			"classDefinition": "com.sap.bpm.wfs.Model",
			"id": "managebp",
			"subject": "manageBP",
			"name": "manageBP",
			"documentation": "CRUD operation for BP",
			"lastIds": "73b68969-dfb8-42eb-9abe-a55d9b611139",
			"events": {
				"1b590713-d20b-4191-8e3d-d3636f4e212f": {
					"name": "StartEvent"
				},
				"c22feb8f-5185-455a-b74d-cd50b566edb8": {
					"name": "EndEvent1"
				},
				"fa5128c0-526c-442e-b3aa-28aeeec5e8c1": {
					"name": "BoundaryTimerEvent1"
				},
				"d26bfeb9-99f7-43e3-8c02-489c593a5b13": {
					"name": "BoundaryTimerEvent2"
				}
			},
			"activities": {
				"ec982866-dd0a-4939-899d-eee5bb5f7c0b": {
					"name": "ExclusiveGateway1"
				},
				"d42edeea-2eda-4627-82ff-7831b0152a87": {
					"name": "CreateAPI"
				},
				"6bfef5cf-f8b5-4e6e-982c-a89b447d3354": {
					"name": "Create Approval"
				},
				"4a560597-b9dd-4718-bb94-2b05e290acf8": {
					"name": "ExclusiveGateway2"
				},
				"283bc1cc-dc14-4915-904d-b0b9d0c44564": {
					"name": "Update Approval"
				},
				"ba36c863-1f6b-45b3-8679-8184c5314191": {
					"name": "ExclusiveGateway3"
				},
				"ba9d5588-9466-4579-b3e6-46536a28dd5a": {
					"name": "Update API"
				},
				"17cde73f-0ebc-41b4-8b17-9858bb7316f8": {
					"name": "View API"
				},
				"d8cf3f24-32e1-4ef9-ada7-1b87e83129db": {
					"name": "MailTask1"
				},
				"48782f01-2bcb-4bf7-879e-d4a95bd28108": {
					"name": "MailTask2"
				}
			},
			"sequenceFlows": {
				"c2906cd3-5d7a-4336-b739-bcacde69a4fd": {
					"name": "SequenceFlow1"
				},
				"3822156d-a911-40ec-92ff-8c790c4314e8": {
					"name": "SequenceFlow2"
				},
				"7d3905f9-9669-4d39-90ad-93c2c1a9d77e": {
					"name": "SequenceFlow3"
				},
				"9e5e0e86-16ae-4c5d-80c7-c2bd9c948a5b": {
					"name": "SequenceFlow5"
				},
				"ca1f3d9c-1b76-4034-9ecb-4f18b71de6ba": {
					"name": "SequenceFlow6"
				},
				"2f42bbcc-f069-4b4b-afc2-cad584f495a9": {
					"name": "SequenceFlow7"
				},
				"3f28eab5-aec8-4d89-a4e1-82fb174c6276": {
					"name": "SequenceFlow8"
				},
				"92f33c86-1c5b-4f48-9888-4d12a2a131f2": {
					"name": "SequenceFlow10"
				},
				"c54285c3-a2c2-4e69-a40d-ab22bb9abdb7": {
					"name": "SequenceFlow11"
				},
				"9074d47e-7b18-46e1-88ba-c70565e6db38": {
					"name": "SequenceFlow12"
				},
				"62516a0a-9748-4677-84f9-6e51a6c9818c": {
					"name": "SequenceFlow13"
				},
				"20b6b754-42f7-4ad9-9d97-63aa79fbea4e": {
					"name": "SequenceFlow14"
				},
				"3a51f01a-afee-4aed-8756-1f8fe1083f13": {
					"name": "SequenceFlow15"
				},
				"e1a1b847-d321-4137-8547-065694291c25": {
					"name": "SequenceFlow16"
				},
				"cdaa8c9d-5ee3-44db-a0f3-da674efab4af": {
					"name": "SequenceFlow17"
				},
				"598bc911-5c91-461f-94e5-04d459b5611f": {
					"name": "SequenceFlow18"
				},
				"7440d0fb-8654-4fb6-b480-daf652666282": {
					"name": "SequenceFlow19"
				}
			},
			"diagrams": {
				"826ad4a8-0d44-46cc-aafa-f3c91dbdd95b": {}
			}
		},
		"1b590713-d20b-4191-8e3d-d3636f4e212f": {
			"classDefinition": "com.sap.bpm.wfs.StartEvent",
			"id": "startevent1",
			"name": "StartEvent"
		},
		"c22feb8f-5185-455a-b74d-cd50b566edb8": {
			"classDefinition": "com.sap.bpm.wfs.EndEvent",
			"id": "endevent1",
			"name": "EndEvent1"
		},
		"fa5128c0-526c-442e-b3aa-28aeeec5e8c1": {
			"classDefinition": "com.sap.bpm.wfs.BoundaryEvent",
			"isCanceling": true,
			"id": "boundarytimerevent1",
			"name": "BoundaryTimerEvent1",
			"attachedToRef": "6bfef5cf-f8b5-4e6e-982c-a89b447d3354",
			"eventDefinitions": {
				"5a7ae941-12a4-402c-8be9-516d80e8655f": {}
			}
		},
		"d26bfeb9-99f7-43e3-8c02-489c593a5b13": {
			"classDefinition": "com.sap.bpm.wfs.BoundaryEvent",
			"isCanceling": true,
			"id": "boundarytimerevent2",
			"name": "BoundaryTimerEvent2",
			"attachedToRef": "283bc1cc-dc14-4915-904d-b0b9d0c44564",
			"eventDefinitions": {
				"63fb6160-2d01-45d0-95d3-eac0d27574d5": {}
			}
		},
		"ec982866-dd0a-4939-899d-eee5bb5f7c0b": {
			"classDefinition": "com.sap.bpm.wfs.ExclusiveGateway",
			"id": "exclusivegateway1",
			"name": "ExclusiveGateway1",
			"default": "20b6b754-42f7-4ad9-9d97-63aa79fbea4e"
		},
		"d42edeea-2eda-4627-82ff-7831b0152a87": {
			"classDefinition": "com.sap.bpm.wfs.ServiceTask",
			"destination": "FS1",
			"path": "/sap/opu/odata/sap/API_BUSINESS_PARTNER/A_BusinessPartner",
			"httpMethod": "POST",
			"xsrfPath": "/sap/opu/odata/sap/ZBP_MDM_LOOKUPS_SRV/StoreFormatSet",
			"requestVariable": "${context.request.data}",
			"responseVariable": "${context.response}",
			"id": "servicetask1",
			"name": "CreateAPI"
		},
		"6bfef5cf-f8b5-4e6e-982c-a89b447d3354": {
			"classDefinition": "com.sap.bpm.wfs.UserTask",
			"subject": "Create Approval",
			"priority": "MEDIUM",
			"isHiddenInLogForParticipant": false,
			"userInterface": "sapui5://html5apps/bpworkflow/bpcreateA/webapp/bpcreateA",
			"recipientUsers": "P1942915070",
			"userInterfaceParams": [],
			"id": "usertask1",
			"name": "Create Approval",
			"dueDateRef": "5a7ae941-12a4-402c-8be9-516d80e8655f"
		},
		"4a560597-b9dd-4718-bb94-2b05e290acf8": {
			"classDefinition": "com.sap.bpm.wfs.ExclusiveGateway",
			"id": "exclusivegateway2",
			"name": "ExclusiveGateway2",
			"default": "ca1f3d9c-1b76-4034-9ecb-4f18b71de6ba"
		},
		"283bc1cc-dc14-4915-904d-b0b9d0c44564": {
			"classDefinition": "com.sap.bpm.wfs.UserTask",
			"subject": "Update Approval",
			"priority": "MEDIUM",
			"isHiddenInLogForParticipant": false,
			"userInterface": "sapui5://html5apps/bpworkflow/bpcreateA/webapp/bpcreateA",
			"recipientUsers": "P1942915070",
			"id": "usertask2",
			"name": "Update Approval",
			"dueDateRef": "fd609d13-a492-49e1-8d55-7a6bf057cc42"
		},
		"ba36c863-1f6b-45b3-8679-8184c5314191": {
			"classDefinition": "com.sap.bpm.wfs.ExclusiveGateway",
			"id": "exclusivegateway3",
			"name": "ExclusiveGateway3",
			"default": "c54285c3-a2c2-4e69-a40d-ab22bb9abdb7"
		},
		"ba9d5588-9466-4579-b3e6-46536a28dd5a": {
			"classDefinition": "com.sap.bpm.wfs.ServiceTask",
			"destination": "FS1",
			"path": "/sap/opu/odata/sap/API_BUSINESS_PARTNER/A_BusinessPartner('${context.request.data.BusinessPartner}')",
			"httpMethod": "PUT",
			"xsrfPath": "/sap/opu/odata/sap/ZBP_MDM_LOOKUPS_SRV/StoreFormatSet",
			"requestVariable": "${context.request.data}",
			"responseVariable": "${context.response}",
			"id": "servicetask2",
			"name": "Update API"
		},
		"17cde73f-0ebc-41b4-8b17-9858bb7316f8": {
			"classDefinition": "com.sap.bpm.wfs.ServiceTask",
			"destination": "FS1",
			"path": "/sap/opu/odata/sap/API_BUSINESS_PARTNER/A_BusinessPartner(BusinessPartner='${context.request.BusinessPartner}')?$format=json&$expand=to_BuPaIdentification,to_BusinessPartnerAddress,to_BusinessPartnerAddress/to_EmailAddress,to_BusinessPartnerAddress/to_FaxNumber,to_BusinessPartnerAddress/to_MobilePhoneNumber,to_BusinessPartnerAddress/to_PhoneNumber,to_BusinessPartnerAddress/to_URLAddress,to_BusinessPartnerBank,to_BusinessPartnerContact,to_BusinessPartnerContact/to_ContactAddress,to_BusinessPartnerContact/to_ContactRelationship,to_BusinessPartnerRole,to_BusinessPartnerTax,to_Customer,to_Customer/to_CustomerCompany,to_Customer/to_CustomerCompany/to_CustomerDunning,to_Customer/to_CustomerCompany/to_WithHoldingTax,to_Customer/to_CustomerSalesArea,to_Customer/to_CustomerSalesArea/to_PartnerFunction,to_Customer/to_CustomerSalesArea/to_SalesAreaTax,to_Supplier,to_Supplier/to_SupplierCompany,to_Supplier/to_SupplierCompany/to_SupplierDunning,to_Supplier/to_SupplierCompany/to_SupplierWithHoldingTax,to_Supplier/to_SupplierPurchasingOrg,to_Supplier/to_SupplierPurchasingOrg/to_PartnerFunction",
			"httpMethod": "GET",
			"responseVariable": "${context.response}",
			"id": "servicetask3",
			"name": "View API"
		},
		"d8cf3f24-32e1-4ef9-ada7-1b87e83129db": {
			"classDefinition": "com.sap.bpm.wfs.MailTask",
			"id": "mailtask1",
			"name": "MailTask1",
			"mailDefinitionRef": "2ac4c05f-3c6b-4def-b732-f80cb14707d3"
		},
		"48782f01-2bcb-4bf7-879e-d4a95bd28108": {
			"classDefinition": "com.sap.bpm.wfs.MailTask",
			"id": "mailtask2",
			"name": "MailTask2",
			"mailDefinitionRef": "95bf55b0-844d-4527-a9b3-f1f5147fd00d"
		},
		"c2906cd3-5d7a-4336-b739-bcacde69a4fd": {
			"classDefinition": "com.sap.bpm.wfs.SequenceFlow",
			"id": "sequenceflow1",
			"name": "SequenceFlow1",
			"sourceRef": "1b590713-d20b-4191-8e3d-d3636f4e212f",
			"targetRef": "ec982866-dd0a-4939-899d-eee5bb5f7c0b"
		},
		"3822156d-a911-40ec-92ff-8c790c4314e8": {
			"classDefinition": "com.sap.bpm.wfs.SequenceFlow",
			"condition": "${context.actionCreate== true}",
			"id": "sequenceflow2",
			"name": "SequenceFlow2",
			"sourceRef": "ec982866-dd0a-4939-899d-eee5bb5f7c0b",
			"targetRef": "6bfef5cf-f8b5-4e6e-982c-a89b447d3354"
		},
		"7d3905f9-9669-4d39-90ad-93c2c1a9d77e": {
			"classDefinition": "com.sap.bpm.wfs.SequenceFlow",
			"id": "sequenceflow3",
			"name": "SequenceFlow3",
			"sourceRef": "d42edeea-2eda-4627-82ff-7831b0152a87",
			"targetRef": "c22feb8f-5185-455a-b74d-cd50b566edb8"
		},
		"9e5e0e86-16ae-4c5d-80c7-c2bd9c948a5b": {
			"classDefinition": "com.sap.bpm.wfs.SequenceFlow",
			"id": "sequenceflow5",
			"name": "SequenceFlow5",
			"sourceRef": "6bfef5cf-f8b5-4e6e-982c-a89b447d3354",
			"targetRef": "4a560597-b9dd-4718-bb94-2b05e290acf8"
		},
		"ca1f3d9c-1b76-4034-9ecb-4f18b71de6ba": {
			"classDefinition": "com.sap.bpm.wfs.SequenceFlow",
			"id": "sequenceflow6",
			"name": "SequenceFlow6",
			"sourceRef": "4a560597-b9dd-4718-bb94-2b05e290acf8",
			"targetRef": "d42edeea-2eda-4627-82ff-7831b0152a87"
		},
		"2f42bbcc-f069-4b4b-afc2-cad584f495a9": {
			"classDefinition": "com.sap.bpm.wfs.SequenceFlow",
			"condition": "${usertasks.usertask1.last.decision == 'reject'}",
			"id": "sequenceflow7",
			"name": "SequenceFlow7",
			"sourceRef": "4a560597-b9dd-4718-bb94-2b05e290acf8",
			"targetRef": "c22feb8f-5185-455a-b74d-cd50b566edb8"
		},
		"3f28eab5-aec8-4d89-a4e1-82fb174c6276": {
			"classDefinition": "com.sap.bpm.wfs.SequenceFlow",
			"condition": "${context.actionCreate == false}",
			"id": "sequenceflow8",
			"name": "SequenceFlow8",
			"sourceRef": "ec982866-dd0a-4939-899d-eee5bb5f7c0b",
			"targetRef": "283bc1cc-dc14-4915-904d-b0b9d0c44564"
		},
		"92f33c86-1c5b-4f48-9888-4d12a2a131f2": {
			"classDefinition": "com.sap.bpm.wfs.SequenceFlow",
			"id": "sequenceflow10",
			"name": "SequenceFlow10",
			"sourceRef": "283bc1cc-dc14-4915-904d-b0b9d0c44564",
			"targetRef": "ba36c863-1f6b-45b3-8679-8184c5314191"
		},
		"c54285c3-a2c2-4e69-a40d-ab22bb9abdb7": {
			"classDefinition": "com.sap.bpm.wfs.SequenceFlow",
			"id": "sequenceflow11",
			"name": "SequenceFlow11",
			"sourceRef": "ba36c863-1f6b-45b3-8679-8184c5314191",
			"targetRef": "ba9d5588-9466-4579-b3e6-46536a28dd5a"
		},
		"9074d47e-7b18-46e1-88ba-c70565e6db38": {
			"classDefinition": "com.sap.bpm.wfs.SequenceFlow",
			"condition": "${usertasks.usertask2.last.decision == 'reject'}",
			"id": "sequenceflow12",
			"name": "SequenceFlow12",
			"sourceRef": "ba36c863-1f6b-45b3-8679-8184c5314191",
			"targetRef": "c22feb8f-5185-455a-b74d-cd50b566edb8"
		},
		"62516a0a-9748-4677-84f9-6e51a6c9818c": {
			"classDefinition": "com.sap.bpm.wfs.SequenceFlow",
			"id": "sequenceflow13",
			"name": "SequenceFlow13",
			"sourceRef": "ba9d5588-9466-4579-b3e6-46536a28dd5a",
			"targetRef": "c22feb8f-5185-455a-b74d-cd50b566edb8"
		},
		"20b6b754-42f7-4ad9-9d97-63aa79fbea4e": {
			"classDefinition": "com.sap.bpm.wfs.SequenceFlow",
			"id": "sequenceflow14",
			"name": "SequenceFlow14",
			"sourceRef": "ec982866-dd0a-4939-899d-eee5bb5f7c0b",
			"targetRef": "17cde73f-0ebc-41b4-8b17-9858bb7316f8"
		},
		"3a51f01a-afee-4aed-8756-1f8fe1083f13": {
			"classDefinition": "com.sap.bpm.wfs.SequenceFlow",
			"id": "sequenceflow15",
			"name": "SequenceFlow15",
			"sourceRef": "17cde73f-0ebc-41b4-8b17-9858bb7316f8",
			"targetRef": "c22feb8f-5185-455a-b74d-cd50b566edb8"
		},
		"e1a1b847-d321-4137-8547-065694291c25": {
			"classDefinition": "com.sap.bpm.wfs.SequenceFlow",
			"id": "sequenceflow16",
			"name": "SequenceFlow16",
			"sourceRef": "fa5128c0-526c-442e-b3aa-28aeeec5e8c1",
			"targetRef": "d8cf3f24-32e1-4ef9-ada7-1b87e83129db"
		},
		"cdaa8c9d-5ee3-44db-a0f3-da674efab4af": {
			"classDefinition": "com.sap.bpm.wfs.SequenceFlow",
			"id": "sequenceflow17",
			"name": "SequenceFlow17",
			"sourceRef": "d8cf3f24-32e1-4ef9-ada7-1b87e83129db",
			"targetRef": "6bfef5cf-f8b5-4e6e-982c-a89b447d3354"
		},
		"598bc911-5c91-461f-94e5-04d459b5611f": {
			"classDefinition": "com.sap.bpm.wfs.SequenceFlow",
			"id": "sequenceflow18",
			"name": "SequenceFlow18",
			"sourceRef": "d26bfeb9-99f7-43e3-8c02-489c593a5b13",
			"targetRef": "48782f01-2bcb-4bf7-879e-d4a95bd28108"
		},
		"7440d0fb-8654-4fb6-b480-daf652666282": {
			"classDefinition": "com.sap.bpm.wfs.SequenceFlow",
			"id": "sequenceflow19",
			"name": "SequenceFlow19",
			"sourceRef": "48782f01-2bcb-4bf7-879e-d4a95bd28108",
			"targetRef": "283bc1cc-dc14-4915-904d-b0b9d0c44564"
		},
		"826ad4a8-0d44-46cc-aafa-f3c91dbdd95b": {
			"classDefinition": "com.sap.bpm.wfs.ui.Diagram",
			"symbols": {
				"bb5ffb4b-b764-41a1-9977-acdf9b28c15f": {},
				"d0855a98-c350-4974-aeef-c2546ecccb1c": {},
				"89955e74-dfe1-4b31-a8b6-ef9bd410b106": {},
				"79df4600-509e-49db-9a15-246dea9ed3cb": {},
				"c0fda35d-2c40-46e9-8cbf-ccd4b29c925b": {},
				"46416c2f-d51a-401b-aad7-48a6dda44e85": {},
				"45c30956-f395-4c1b-ab5c-4c8ed8b557df": {},
				"75776a9f-2c32-4d58-8ecc-53a8f7af920d": {},
				"7a4f1ea7-808b-450d-b205-7c6f3a0d93cb": {},
				"5837ea10-5be0-47d5-919a-2dd93f43e607": {},
				"454b2f82-0eda-429a-a912-2cfe2aada2a4": {},
				"59d1a21b-ec4d-4f7c-91b9-89ce36b6c5f6": {},
				"2711b087-943c-45d7-a1aa-826d5cb8383b": {},
				"0dc470da-56b4-406d-bd5c-b74035211a11": {},
				"2f2935d4-8f7b-4101-9c95-ec262739b9ac": {},
				"5e280de1-3843-4f5f-93c3-93d212806154": {},
				"4e3f53c2-b86a-42c0-8a23-5a94933654c1": {},
				"e15d8542-b409-49bb-a241-2b926d5af69f": {},
				"eb8b7b4e-3be5-43b3-b098-14b1c151f8fb": {},
				"e6124f0d-e878-4436-8e30-fe8f4fcd9d6a": {},
				"f7f390a0-11ba-44f7-a067-a57f43d1c223": {},
				"f1b391ca-6fc3-4d89-8450-51c146432662": {},
				"216cc0ed-ae73-4c03-8e9a-1ff53ef2c41b": {},
				"c1320ed4-6c01-4367-aaad-1eb8ed72da65": {},
				"9cdf7056-9ea3-426e-9416-f8d04cfd6608": {},
				"fe2d742d-7479-448d-b95d-a1c5c46f0f5b": {},
				"d3917278-570a-462f-8728-c3437e18e78d": {},
				"07f5e1a7-d211-42de-903e-7e7e36b7d9c6": {},
				"b6e7b4a9-5a4a-4738-8c6d-b69cf428ebf9": {}
			}
		},
		"5a7ae941-12a4-402c-8be9-516d80e8655f": {
			"classDefinition": "com.sap.bpm.wfs.TimerEventDefinition",
			"timeDuration": "PT1M",
			"id": "timereventdefinition2"
		},
		"63fb6160-2d01-45d0-95d3-eac0d27574d5": {
			"classDefinition": "com.sap.bpm.wfs.TimerEventDefinition",
			"timeDuration": "PT2M",
			"id": "timereventdefinition4"
		},
		"bb5ffb4b-b764-41a1-9977-acdf9b28c15f": {
			"classDefinition": "com.sap.bpm.wfs.ui.StartEventSymbol",
			"x": 31,
			"y": 363,
			"width": 32,
			"height": 32,
			"object": "1b590713-d20b-4191-8e3d-d3636f4e212f"
		},
		"d0855a98-c350-4974-aeef-c2546ecccb1c": {
			"classDefinition": "com.sap.bpm.wfs.ui.EndEventSymbol",
			"x": 940,
			"y": 347,
			"width": 35,
			"height": 35,
			"object": "c22feb8f-5185-455a-b74d-cd50b566edb8"
		},
		"89955e74-dfe1-4b31-a8b6-ef9bd410b106": {
			"classDefinition": "com.sap.bpm.wfs.ui.SequenceFlowSymbol",
			"points": "63,377.5 143,377.5",
			"sourceSymbol": "bb5ffb4b-b764-41a1-9977-acdf9b28c15f",
			"targetSymbol": "79df4600-509e-49db-9a15-246dea9ed3cb",
			"object": "c2906cd3-5d7a-4336-b739-bcacde69a4fd"
		},
		"79df4600-509e-49db-9a15-246dea9ed3cb": {
			"classDefinition": "com.sap.bpm.wfs.ui.ExclusiveGatewaySymbol",
			"x": 122,
			"y": 353,
			"object": "ec982866-dd0a-4939-899d-eee5bb5f7c0b"
		},
		"c0fda35d-2c40-46e9-8cbf-ccd4b29c925b": {
			"classDefinition": "com.sap.bpm.wfs.ui.SequenceFlowSymbol",
			"points": "144,376.5 144,183 431.5,183",
			"sourceSymbol": "79df4600-509e-49db-9a15-246dea9ed3cb",
			"targetSymbol": "75776a9f-2c32-4d58-8ecc-53a8f7af920d",
			"object": "3822156d-a911-40ec-92ff-8c790c4314e8"
		},
		"46416c2f-d51a-401b-aad7-48a6dda44e85": {
			"classDefinition": "com.sap.bpm.wfs.ui.ServiceTaskSymbol",
			"x": 705,
			"y": 93,
			"width": 100,
			"height": 60,
			"object": "d42edeea-2eda-4627-82ff-7831b0152a87"
		},
		"45c30956-f395-4c1b-ab5c-4c8ed8b557df": {
			"classDefinition": "com.sap.bpm.wfs.ui.SequenceFlowSymbol",
			"points": "798,120 957.5,120 957.5,364.5",
			"sourceSymbol": "46416c2f-d51a-401b-aad7-48a6dda44e85",
			"targetSymbol": "d0855a98-c350-4974-aeef-c2546ecccb1c",
			"object": "7d3905f9-9669-4d39-90ad-93c2c1a9d77e"
		},
		"75776a9f-2c32-4d58-8ecc-53a8f7af920d": {
			"classDefinition": "com.sap.bpm.wfs.ui.UserTaskSymbol",
			"x": 417,
			"y": 152,
			"width": 100,
			"height": 60,
			"object": "6bfef5cf-f8b5-4e6e-982c-a89b447d3354",
			"symbols": {
				"d3e9a59c-c1fc-413d-8459-6ec355248405": {}
			}
		},
		"7a4f1ea7-808b-450d-b205-7c6f3a0d93cb": {
			"classDefinition": "com.sap.bpm.wfs.ui.SequenceFlowSymbol",
			"points": "467,182 642,182",
			"sourceSymbol": "75776a9f-2c32-4d58-8ecc-53a8f7af920d",
			"targetSymbol": "5837ea10-5be0-47d5-919a-2dd93f43e607",
			"object": "9e5e0e86-16ae-4c5d-80c7-c2bd9c948a5b"
		},
		"5837ea10-5be0-47d5-919a-2dd93f43e607": {
			"classDefinition": "com.sap.bpm.wfs.ui.ExclusiveGatewaySymbol",
			"x": 621,
			"y": 161,
			"object": "4a560597-b9dd-4718-bb94-2b05e290acf8"
		},
		"454b2f82-0eda-429a-a912-2cfe2aada2a4": {
			"classDefinition": "com.sap.bpm.wfs.ui.SequenceFlowSymbol",
			"points": "642,182 642,123 755,123",
			"sourceSymbol": "5837ea10-5be0-47d5-919a-2dd93f43e607",
			"targetSymbol": "46416c2f-d51a-401b-aad7-48a6dda44e85",
			"object": "ca1f3d9c-1b76-4034-9ecb-4f18b71de6ba"
		},
		"59d1a21b-ec4d-4f7c-91b9-89ce36b6c5f6": {
			"classDefinition": "com.sap.bpm.wfs.ui.SequenceFlowSymbol",
			"points": "642,182 801.5,182 801.5,356 961,356",
			"sourceSymbol": "5837ea10-5be0-47d5-919a-2dd93f43e607",
			"targetSymbol": "d0855a98-c350-4974-aeef-c2546ecccb1c",
			"object": "2f42bbcc-f069-4b4b-afc2-cad584f495a9"
		},
		"2711b087-943c-45d7-a1aa-826d5cb8383b": {
			"classDefinition": "com.sap.bpm.wfs.ui.SequenceFlowSymbol",
			"points": "143,374 307.5,374 307.5,385 454.5,385",
			"sourceSymbol": "79df4600-509e-49db-9a15-246dea9ed3cb",
			"targetSymbol": "0dc470da-56b4-406d-bd5c-b74035211a11",
			"object": "3f28eab5-aec8-4d89-a4e1-82fb174c6276"
		},
		"0dc470da-56b4-406d-bd5c-b74035211a11": {
			"classDefinition": "com.sap.bpm.wfs.ui.UserTaskSymbol",
			"x": 451,
			"y": 349,
			"width": 100,
			"height": 60,
			"object": "283bc1cc-dc14-4915-904d-b0b9d0c44564",
			"symbols": {
				"ba14223c-8543-4b91-b563-a22aa06aa493": {}
			}
		},
		"2f2935d4-8f7b-4101-9c95-ec262739b9ac": {
			"classDefinition": "com.sap.bpm.wfs.ui.ExclusiveGatewaySymbol",
			"x": 631,
			"y": 349,
			"object": "ba36c863-1f6b-45b3-8679-8184c5314191"
		},
		"5e280de1-3843-4f5f-93c3-93d212806154": {
			"classDefinition": "com.sap.bpm.wfs.ui.SequenceFlowSymbol",
			"points": "501,376 645,376",
			"sourceSymbol": "0dc470da-56b4-406d-bd5c-b74035211a11",
			"targetSymbol": "2f2935d4-8f7b-4101-9c95-ec262739b9ac",
			"object": "92f33c86-1c5b-4f48-9888-4d12a2a131f2"
		},
		"4e3f53c2-b86a-42c0-8a23-5a94933654c1": {
			"classDefinition": "com.sap.bpm.wfs.ui.ServiceTaskSymbol",
			"x": 735,
			"y": 449,
			"width": 100,
			"height": 60,
			"object": "ba9d5588-9466-4579-b3e6-46536a28dd5a"
		},
		"e15d8542-b409-49bb-a241-2b926d5af69f": {
			"classDefinition": "com.sap.bpm.wfs.ui.SequenceFlowSymbol",
			"points": "652,371.5 652,477.5 735.5,477.5",
			"sourceSymbol": "2f2935d4-8f7b-4101-9c95-ec262739b9ac",
			"targetSymbol": "4e3f53c2-b86a-42c0-8a23-5a94933654c1",
			"object": "c54285c3-a2c2-4e69-a40d-ab22bb9abdb7"
		},
		"eb8b7b4e-3be5-43b3-b098-14b1c151f8fb": {
			"classDefinition": "com.sap.bpm.wfs.ui.SequenceFlowSymbol",
			"points": "673,365.5 940,365.5",
			"sourceSymbol": "2f2935d4-8f7b-4101-9c95-ec262739b9ac",
			"targetSymbol": "d0855a98-c350-4974-aeef-c2546ecccb1c",
			"object": "9074d47e-7b18-46e1-88ba-c70565e6db38"
		},
		"e6124f0d-e878-4436-8e30-fe8f4fcd9d6a": {
			"classDefinition": "com.sap.bpm.wfs.ui.SequenceFlowSymbol",
			"points": "785,479 887.75,479 887.75,361.5 943.75,361.5",
			"sourceSymbol": "4e3f53c2-b86a-42c0-8a23-5a94933654c1",
			"targetSymbol": "d0855a98-c350-4974-aeef-c2546ecccb1c",
			"object": "62516a0a-9748-4677-84f9-6e51a6c9818c"
		},
		"f7f390a0-11ba-44f7-a067-a57f43d1c223": {
			"classDefinition": "com.sap.bpm.wfs.ui.ServiceTaskSymbol",
			"x": 438,
			"y": 568,
			"width": 100,
			"height": 60,
			"object": "17cde73f-0ebc-41b4-8b17-9858bb7316f8"
		},
		"f1b391ca-6fc3-4d89-8450-51c146432662": {
			"classDefinition": "com.sap.bpm.wfs.ui.SequenceFlowSymbol",
			"points": "145,374 145,598 438.5,598",
			"sourceSymbol": "79df4600-509e-49db-9a15-246dea9ed3cb",
			"targetSymbol": "f7f390a0-11ba-44f7-a067-a57f43d1c223",
			"object": "20b6b754-42f7-4ad9-9d97-63aa79fbea4e"
		},
		"216cc0ed-ae73-4c03-8e9a-1ff53ef2c41b": {
			"classDefinition": "com.sap.bpm.wfs.ui.SequenceFlowSymbol",
			"points": "488,598 957.5,598 957.5,381.5",
			"sourceSymbol": "f7f390a0-11ba-44f7-a067-a57f43d1c223",
			"targetSymbol": "d0855a98-c350-4974-aeef-c2546ecccb1c",
			"object": "3a51f01a-afee-4aed-8756-1f8fe1083f13"
		},
		"c1320ed4-6c01-4367-aaad-1eb8ed72da65": {
			"classDefinition": "com.sap.bpm.wfs.ui.MailTaskSymbol",
			"x": 445,
			"y": -30,
			"width": 100,
			"height": 60,
			"object": "d8cf3f24-32e1-4ef9-ada7-1b87e83129db"
		},
		"9cdf7056-9ea3-426e-9416-f8d04cfd6608": {
			"classDefinition": "com.sap.bpm.wfs.ui.SequenceFlowSymbol",
			"points": "451,136 451,83 465.5,83 465.5,29.5",
			"sourceSymbol": "d3e9a59c-c1fc-413d-8459-6ec355248405",
			"targetSymbol": "c1320ed4-6c01-4367-aaad-1eb8ed72da65",
			"object": "e1a1b847-d321-4137-8547-065694291c25"
		},
		"fe2d742d-7479-448d-b95d-a1c5c46f0f5b": {
			"classDefinition": "com.sap.bpm.wfs.ui.SequenceFlowSymbol",
			"points": "515.5,0 515.5,91.25 499.5,91.25 499.5,152.5",
			"sourceSymbol": "c1320ed4-6c01-4367-aaad-1eb8ed72da65",
			"targetSymbol": "75776a9f-2c32-4d58-8ecc-53a8f7af920d",
			"object": "cdaa8c9d-5ee3-44db-a0f3-da674efab4af"
		},
		"d3917278-570a-462f-8728-c3437e18e78d": {
			"classDefinition": "com.sap.bpm.wfs.ui.MailTaskSymbol",
			"x": 451,
			"y": 474,
			"width": 100,
			"height": 60,
			"object": "48782f01-2bcb-4bf7-879e-d4a95bd28108"
		},
		"07f5e1a7-d211-42de-903e-7e7e36b7d9c6": {
			"classDefinition": "com.sap.bpm.wfs.ui.SequenceFlowSymbol",
			"points": "467,425 467,449.5 469,449.5 469,474.5",
			"sourceSymbol": "ba14223c-8543-4b91-b563-a22aa06aa493",
			"targetSymbol": "d3917278-570a-462f-8728-c3437e18e78d",
			"object": "598bc911-5c91-461f-94e5-04d459b5611f"
		},
		"b6e7b4a9-5a4a-4738-8c6d-b69cf428ebf9": {
			"classDefinition": "com.sap.bpm.wfs.ui.SequenceFlowSymbol",
			"points": "535,504 535,401",
			"sourceSymbol": "d3917278-570a-462f-8728-c3437e18e78d",
			"targetSymbol": "0dc470da-56b4-406d-bd5c-b74035211a11",
			"object": "7440d0fb-8654-4fb6-b480-daf652666282"
		},
		"d3e9a59c-c1fc-413d-8459-6ec355248405": {
			"classDefinition": "com.sap.bpm.wfs.ui.BoundaryEventSymbol",
			"x": 435,
			"y": 136,
			"object": "fa5128c0-526c-442e-b3aa-28aeeec5e8c1"
		},
		"ba14223c-8543-4b91-b563-a22aa06aa493": {
			"classDefinition": "com.sap.bpm.wfs.ui.BoundaryEventSymbol",
			"x": 451,
			"y": 393,
			"object": "d26bfeb9-99f7-43e3-8c02-489c593a5b13"
		},
		"73b68969-dfb8-42eb-9abe-a55d9b611139": {
			"classDefinition": "com.sap.bpm.wfs.LastIDs",
			"timereventdefinition": 4,
			"maildefinition": 2,
			"sequenceflow": 19,
			"startevent": 1,
			"boundarytimerevent": 2,
			"endevent": 1,
			"usertask": 2,
			"servicetask": 3,
			"scripttask": 2,
			"mailtask": 2,
			"exclusivegateway": 3
		},
		"2ac4c05f-3c6b-4def-b732-f80cb14707d3": {
			"classDefinition": "com.sap.bpm.wfs.MailDefinition",
			"name": "maildefinition1",
			"to": "${context.user.email}",
			"subject": "Pending Action for Create Business Partner",
			"text": "Test Create Approval required",
			"id": "maildefinition1"
		},
		"fd609d13-a492-49e1-8d55-7a6bf057cc42": {
			"classDefinition": "com.sap.bpm.wfs.TimerEventDefinition",
			"timeDuration": "PT5M",
			"id": "timereventdefinition3"
		},
		"95bf55b0-844d-4527-a9b3-f1f5147fd00d": {
			"classDefinition": "com.sap.bpm.wfs.MailDefinition",
			"name": "maildefinition2",
			"to": "${context.user.email}",
			"subject": "Pending Action for Approval Business Partner",
			"text": "Test Update Approval Business Partner",
			"id": "maildefinition2"
		}
	}
}